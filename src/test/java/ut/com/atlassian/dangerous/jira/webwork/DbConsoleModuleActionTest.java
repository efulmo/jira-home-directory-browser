package ut.com.atlassian.sysadmin.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class DbConsoleModuleActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //DbConsoleModuleAction testClass = new DbConsoleModuleAction();

        throw new Exception("DbConsoleModuleAction has no tests!");

    }

}
