package info.renjithv.jira.plugins.sysadmin.jira.webwork;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.sort;

@WebSudoRequired
public class HomeDirectoryBrowserAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(HomeDirectoryBrowserAction.class);
    private String folder = "";
    private File jiraHome;

    public HomeDirectoryBrowserAction() {
        jiraHome = ComponentAccessor.getComponentOfType(JiraHome.class).getHome();
    }

    @Override
    protected String doExecute() throws Exception {
        return super.doExecute();
    }

    @Override
    public String execute() throws Exception {
        return super.execute(); //returns SUCCESS
    }

    public List<File> getNames(){
        List<File> files = null;
        if(folder.isEmpty()){
            files = Arrays.asList(jiraHome.listFiles());
        }
        else
        {
            files = Arrays.asList(new File(jiraHome.getAbsolutePath() + folder).listFiles());
        }
        sort(files);
        return files;
    }
    public File getParent(){
        return new File(jiraHome.getAbsolutePath() + folder).getParentFile();
    }

    public String getFolder() {
        return folder;
    }

    public List<String> getParts(){
        List<String> strings = new ArrayList(Arrays.asList(getFolder().split("/")));
        if(strings.size() > 0 && (strings.get(0) == null || strings.get(0).isEmpty())){
            strings.remove(0);
        }
        return strings;
    }
    public void setFolder(String folder) {
        this.folder = folder.replaceAll("\\.\\.","").replaceAll("//","/")
                .replace(File.separator, "/");
    }
    public String stripPath(File file){
        String fileName = file.getAbsolutePath();
        if(fileName.startsWith(jiraHome.getAbsolutePath()))
        {
            return fileName.replace(jiraHome.getAbsolutePath(),"").replace(File.separator, "/");
        }
        else {
            return "";
        }
    }
    public static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
