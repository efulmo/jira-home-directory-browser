package info.renjithv.jira.plugins.sysadmin.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.LinkedHashMap;

import static info.renjithv.jira.plugins.sysadmin.rest.PermUtility.isSysAdmin;

/**
 * A resource of message.
 */
@Path("/livelog")
@WebSudoRequired
public class LiveLogRest {
    private static final long MIN_DISPLAY_SIZE = 5120;

    private class ReaderData {
        public ReaderData(RandomAccessFile reader) {
            this.reader = reader;
            this.lastAccessTime = System.currentTimeMillis();
        }

        private RandomAccessFile getReader() {
            return reader;
        }

        private void setReader(RandomAccessFile reader) {
            this.reader = reader;
        }

        RandomAccessFile reader;

        private Long getLastAccessTime() {
            return lastAccessTime;
        }

        Long lastAccessTime;

        public void updateLastAccessTime() {
            this.lastAccessTime = System.currentTimeMillis();
        }
    }

    private static final long CLEAN_INTERVAL = 5 * 60 * 1000;//5 Minutes

    private static LinkedHashMap<String, ReaderData> activeReads = new LinkedHashMap<String, ReaderData>();
    private static final Logger log = LoggerFactory.getLogger(LiveLogRest.class);
    private static long lastCleanedTime = System.currentTimeMillis();
    private boolean cleaningInProgress = false;

    @GET
    @AnonymousAllowed
    @Path("id")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getId(@QueryParam("filename") String filename) {
        if(!isSysAdmin())
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if(null == filename || filename.isEmpty()){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        final ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        final File jiraHome = ComponentAccessor.getComponentOfType(JiraHome.class).getHome();
        final File file = new File(jiraHome.getAbsolutePath() + filename);
        final String id = Long.toHexString(System.nanoTime()) + Integer.toHexString(user.hashCode());
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            if(randomAccessFile.length() > MIN_DISPLAY_SIZE){
                randomAccessFile.seek(randomAccessFile.length() - MIN_DISPLAY_SIZE);
            }
            synchronized (activeReads) {
                activeReads.put(id, new ReaderData(randomAccessFile));
            }
        } catch (FileNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (IOException e) {
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        }
        cleanReaders();
        return Response.ok(new LogDataModel(id)).build();
    }

    @GET
    @AnonymousAllowed
    @Path("data")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getData(@QueryParam("id") String id) {
        final ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        if(!isSysAdmin())
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        else if (id == null ) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        LogDataModel dataModel = new LogDataModel(id);

        synchronized (activeReads) {
            if (!activeReads.containsKey(id)) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            else if(id.endsWith(String.valueOf(user.hashCode()))){
                //Id found, but not this user's
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        }
        RandomAccessFile reader;
        synchronized (activeReads) {
            reader = activeReads.get(id).getReader();
        }
        try {

            while (true) {
                String line = reader.readLine();
                if (null == line) {
                    break;
                }
                if(!line.isEmpty()){
                    dataModel.addData(line);
                }
            }
            synchronized (activeReads) {
                //Move the recently used to end
                final ReaderData accessData = activeReads.remove(id);
                accessData.updateLastAccessTime();
                activeReads.put(id,accessData);
            }
        }
        catch (EOFException eof){
            //do nothing
            log.warn("Eof exception");
        }
        catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        cleanReaders();
        return Response.ok(dataModel).build();
    }

    @GET
    @AnonymousAllowed
    @Path("close")
    @Produces({MediaType.APPLICATION_JSON})
    public Response close(@QueryParam("id") String id) {
        final ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        if (!ComponentAccessor.getPermissionManager().hasPermission(Permissions.SYSTEM_ADMIN, user)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        } else if (id.endsWith(String.valueOf(user.hashCode()))) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        synchronized (activeReads) {

            if (activeReads.containsKey(id)) {
                RandomAccessFile reader = activeReads.get(id).getReader();
                try {
                    reader.close();
                    activeReads.remove(id);
                    log.debug("Closing reader - page unload");
                } catch (IOException e) {
                    log.error("Unable to close reader - " + e);
                }
            }
        }
        cleanReaders();
        return Response.ok(String.valueOf(System.nanoTime()) + user.hashCode()).build();
    }

    private void cleanReaders() {
        if (System.currentTimeMillis() - lastCleanedTime > CLEAN_INTERVAL && !cleaningInProgress) {
            cleaningInProgress = true;
            synchronized (activeReads) {
                lastCleanedTime = System.currentTimeMillis();
                for (ReaderData data : activeReads.values()) {
                    if (System.currentTimeMillis() - data.getLastAccessTime() > CLEAN_INTERVAL) {
                        RandomAccessFile reader = data.getReader();
                        try {
                            reader.close();
                            log.debug("Closing reader - clean");
                            activeReads.remove(data);
                        } catch (IOException e) {
                            log.error("Unable to close reader while cleaning - " + e);
                        }
                    } else {
                        break;//We can break here since the recently used ones are at the end
                    }
                }
            }
            cleaningInProgress = false;
        }
    }
}