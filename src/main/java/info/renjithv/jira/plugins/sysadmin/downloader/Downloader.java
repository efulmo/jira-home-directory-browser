package info.renjithv.jira.plugins.sysadmin.downloader;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static info.renjithv.jira.plugins.sysadmin.rest.PermUtility.isSysAdmin;

public class Downloader extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(Downloader.class);
    private static final int FILEBUFFERSIZE = 2048;
    File jiraHome = ComponentAccessor.getComponentOfType(JiraHome.class).getHome();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException {
        if (!isSysAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        String fileName = req.getParameter("filename");
        if (null != fileName) {
            fileName = fileName.replaceAll("\\.\\.", "").replaceAll("//", "/");
            if(fileName.isEmpty()){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            String mimeType = "application/octet-stream";
            response.setContentType(mimeType);
            InputStream in = null;
            ServletOutputStream out = null;

            try {
                out = response.getOutputStream();
                final String mode = req.getParameter("mode");
                if (mode != null && !mode.isEmpty() && mode.equals("zip")) {
                    File zipFile = createZip(fileName);
                    in = new FileInputStream(zipFile);
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFile.getName() + "\"");
                } else {
                    in = new FileInputStream(jiraHome.getAbsolutePath() + fileName);
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + new File(fileName).getName() + "\"");
                }
                byte[] bytes = new byte[FILEBUFFERSIZE];
                int bytesRead;


                while ((bytesRead = in.read(bytes)) != -1) {
                    out.write(bytes, 0, bytesRead);
                }
            } catch (FileNotFoundException e) {
                log.error("Unknown file requested - " + fileName);
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            } catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    protected File createZip(String fileName) throws IOException {
        final String zipFileName = jiraHome.getAbsolutePath() + fileName + String.valueOf(System.currentTimeMillis()) + ".zip";
        final File fileZip = new File(zipFileName);
        final ZipOutputStream outZip = new ZipOutputStream(new FileOutputStream(fileZip));
        InputStream in = new FileInputStream(jiraHome.getAbsolutePath() + fileName);

        ZipEntry e = new ZipEntry(fileName);
        outZip.putNextEntry(e);
        byte[] bytes = new byte[FILEBUFFERSIZE];
        int bytesRead;
        while ((bytesRead = in.read(bytes)) != -1) {
            outZip.write(bytes, 0, bytesRead);
        }
        outZip.closeEntry();
        outZip.close();
        return fileZip;
    }
}